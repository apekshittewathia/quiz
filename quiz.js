function QuizMaker(quizId) {
  this.$quizMaker = $("[data-id=" + quizId + "]");
}

QuizMaker.errorMessages = {
  NUMBER_OF_QUESTIONS: "Number of Questions can't be 0 or empty",
  OPERATORS: "Select atleast one operator",
  RANGE: "Please specify both Min and Max values"
};

QuizMaker.prototype.bindEvents = function() {
  var that = this;
  this.$quizMaker.find("[data-type=add-quiz]").click(function(event) {
    event.preventDefault();
    that.addQuiz();
  });
};

QuizMaker.prototype.validate = function(options) {
  if (!options.numberOfQuestions) {
    alert(QuizMaker.errorMessages.NUMBER_OF_QUESTIONS);
  } else if (options.operators.length === 0) {
    alert(QuizMaker.errorMessages.OPERATORS);
  } else if (!options.minimumOperand || !options.maximumOperand) {
    alert(QuizMaker.errorMessages.RANGE);
  } else {
    return true;
  }
  return false;
};

QuizMaker.prototype.makeQuestionStructure = function() {
  var $questionDiv = $("<div class='question-div' data-type='question-div'></div>");
  $questionDiv.append("<div data-type='question'></div>");
  $questionDiv.append("<label for='answer'>ANSWER:</label>");
  $questionDiv.append("<input type='text' id='answer' data-type='answer'>");
  $questionDiv.append("<button id='next' data-type='next'>Next Question</button>");
  $questionDiv.append("<div data-type='score'></div>")
  return $questionDiv;
};

QuizMaker.prototype.addQuiz = function() {
  var options = {
    $questionDiv: this.makeQuestionStructure(),
    numberOfQuestions: this.$quizMaker.find("input[data-type=number-of-questions]").val(),
    operators: this.$quizMaker.find("select[data-type=operators]")[0].selectedOptions,
    minimumOperand: this.$quizMaker.find("input[data-type=min-value]").val(),
    maximumOperand: this.$quizMaker.find("input[data-type=max-value]").val()
  };
  if (this.validate(options)) {
    var quiz = new Quiz(options);
    this.$quizMaker.append(quiz.$questionDiv);
    quiz.addQuestion();
    quiz.bindEvents();
  }
};

//-----------------------------------

function Quiz(options) {
  this.score = 0;
  this.questionNumber = 0;
  this.$questionDiv = options.$questionDiv;
  this.$scoreDiv = this.$questionDiv.find("[data-type=score]");
  this.numberOfQuestions = parseInt(options.numberOfQuestions);
  this.operators = options.operators;
  this.minimumOperand = parseInt(options.minimumOperand);
  this.maximumOperand = parseInt(options.maximumOperand);
  this.questionsArray = []
}

// Returns a random integer between min (include) and max (include)
Quiz.prototype.getRandomNumberBetween = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

Quiz.prototype.addQuestion = function() {
  var operatorNumber = this.getRandomNumberBetween(0, this.operators.length - 1);
  var currentQuestion = new Question(this.getRandomNumberBetween(this.minimumOperand,
      this.maximumOperand), this.operators[operatorNumber].getAttribute("data-value"),
    this.getRandomNumberBetween(this.minimumOperand, this.maximumOperand));
  this.$questionDiv.children("div[data-type=question]").text(currentQuestion.questionText);
  this.questionsArray.push(currentQuestion);
};

Quiz.prototype.showResult = function() {
  this.$questionDiv.children().remove();
  this.$questionDiv.append("Score:" + this.score + "/" + this.numberOfQuestions + "<br>");
  var wrongQuestions = this.questionsArray.filter(function(item, index, array) {
    return !item.isRight;
  });
  if (wrongQuestions.length > 0) {
    this.$questionDiv.append("Wrong Questions And their Answers:<br>");
  }
  var that = this;
  wrongQuestions.forEach(function(item, index, array) {
    that.$questionDiv.append("Question:" + item.questionText + "<br>Answer:" + item.answer + "<br>");
  });
};

Quiz.prototype.showScore = function() {
  this.$scoreDiv.html("Score:" + this.score + "/" + (this.questionNumber + 1));
};

Quiz.prototype.getAnswer = function() {
  return parseInt(this.$questionDiv.children("[data-type=answer]").val());
};

Quiz.prototype.bindEvents = function() {
  var that = this;
  this.$questionDiv.on("click", "button[data-type=next]", function(event) {
    event.preventDefault();
    var $this = $(this);
    var currentQuestion = that.questionsArray[that.questionNumber];
    if (currentQuestion.isRight = (that.getAnswer() === currentQuestion.answer)) {
      that.score++;
    }
    that.showScore();
    that.questionNumber++;
    that.questionNumber < that.numberOfQuestions ? that.addQuestion() : that.showResult();
  });
};

//-----------------------------------

function Question(operand1, operator, operand2) {
  this.operand1 = operand1;
  this.operand2 = operand2;
  this.operator = operator;
  this.questionText = this.generateQuestionText();
  this.answer = eval(this.questionText);
}

Question.prototype.generateQuestionText = function() {
  return this.operand1 + this.operator + this.operand2;
};

$(document).ready(function() {
  var quizMaker = new QuizMaker("form1");
  quizMaker.bindEvents();

  var quizMaker2 = new QuizMaker("form2");
  quizMaker2.bindEvents();

});